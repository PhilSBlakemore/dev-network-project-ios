//
//  CustomCollectionViewCell.swift
//  dev-network-project-ios
//
//  Created by Philip Blakemore on 22/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        cellImageView = UIImageView(frame: contentView.frame)
        cellImageView.contentMode = .scaleAspectFit
        cellImageView.clipsToBounds = true
        contentView.addSubview(cellImageView)
    }
}
