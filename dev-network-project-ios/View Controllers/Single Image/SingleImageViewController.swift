//
//  SingleImageViewController.swift
//  dev-network-project-ios
//
//  Created by Philip Blakemore on 22/01/2018.
//  Copyright © 2018 Phil Blakemore. All rights reserved.
//

import UIKit

class SingleImageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func setUpPageController() {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = 12
        pageControl.currentPageIndicatorTintColor = UIColor.red
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        view.addSubview(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -20).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
}
